<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SobreController;
use App\Http\Controllers\ServicoController;
use App\Http\Controllers\ServicoUmController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ContatoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
});

Route::get('home', [HomeController::class, 'index']);
Route::get('sobre', [SobreController::class, 'index']);
Route::get('servico', [ServicoController::class, 'index']);

Route::get('servico-um', [ServicoUmController::class, 'index']);


Route::get('cliente', [ClienteController::class, 'index']);
Route::get('contato', [ContatoController::class, 'index']);
