@extends('template.layout')
@section('conteudo')

<div class="page-title-area bg-22">
    <div class="container">
        <div class="page-title-content">
            <h2>Sobre Re Security</h2>
            <ul>
                <li><a href="home">Home</a></li>
                <li>Sobre</li>
            </ul>
        </div>
    </div>
</div>
<section class="complete-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="complete-img"></div>
            </div>
            <div class="col-lg-6">
                <div class="complete-content">
                    <h2>The most Complete Effective Protection for Your Home &amp; Office</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor elit incididunt ut labore et dolore magna aliqua. Quis ipsum</p>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security"><i class="flaticon-order"></i>
                                <h3>Check and Search Hazards</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security"><i class="flaticon-anti-virus-software"></i>
                                <h3>Install &amp; Configure Software</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security mb-0 mb-rs-need"><i class="flaticon-scientist"></i>
                                <h3>Departure Our Experts</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security mb-0"><i class="flaticon-technical-support"></i>
                                <h3>24/7 Support</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="complete-shape"><img src="img/complete-shape.png" alt="Image"/></div>
</section>


<section class="electronic-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="electronic-content">
                    <h2>Innovative Electronic Protection of Your Office and Home Control Online</h2>
                    <div class="electronic-tab-wrap">
                        <div class="tab electronic-tab">
                            <ul class="tabs">
                                <li class="current">Intercom System</li>
                                <li>CCTV</li>
                                <li>GDPR</li>
                                <li>Encryption</li>
                                <li>Our Goal</li>
                            </ul>
                            <div class="tab_content">
                                <div id="tab1" class="tabs_item">
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo ducimus vero, vero corporis voluptates beatae pariatur laudantium, fugiat illum ab deserunt nostrum aliquid quisquam esse? Voluptatibus quia velit numquam esse porro ipsum dolor, sit amet consectetur adipisicing elit. Illo ducimus vero, corporis.</p>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis, soluta, aspernatur dolorum sequi quisquam ullam in pariatur nihil dolorem cumque excepturi totam. Qui excepturi quasi cumque placeat fuga. Ea, eius?</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab2" class="tabs_item">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum</p>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab3" class="tabs_item">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab4" class="tabs_item">
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab5" class="tabs_item">
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique</p>
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="electronic-img"><img src="img/electronic-img.png" alt="Image"/></div>
            </div>
        </div>
    </div>
</section>

<div class="pt-100">
    <section class="approach-area pb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="approach-img"><img src="img/approach-img.jpg" alt="Image"/></div>
                </div>
                <div class="col-lg-6">
                    <div class="approach-content">
                        <h2>Our Approach To Security</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv</p>
                        <ul>
                            <li><i class="flaticon-cyber"></i>
                                <h3>Secure by Design</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                            </li>
                            <li><i class="flaticon-cyber-security"></i>
                                <h3>Compliant by Design</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                            </li>
                            <li><i class="flaticon-profile"></i>
                                <h3>Continuous Monitoring</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<section class="client-area ptb-100">
    <div class="container">
        <div class="section-title white-title">
            <h2>What Client’s Say About Us</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima.</p>
        </div>
    </div>
</section>

@endsection
