@extends('template.layout')
@section('conteudo')

<section class="banner-area banner-item-bg-1 jarallax">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-9">
                <div class="banner-text"><span>Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
                    <h1>Lorem ipsum dolor sit</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil architecto laborum eaque! Deserunt maxime, minus quas molestiae reiciendis esse natus nisi iure.</p>
                    <div class="banner-btn"><a class="default-btn" href="#">Serviços</a><a class="default-btn active" href="about.html">Sobre</a></div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="video-btn-animat one"><a class="video-btn popup-youtube" href="#play-video"><i class="bx bx-play"></i></a></div>
            </div>
        </div>
    </div>
    <div class="container pt-100">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="single-features">
                    <h3><i class="bx bx-check-shield"></i>INSTALAÇÃO</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                    <span class="bx bx-check-shield"></span></div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="single-features">
                    <h3><i class="bx bx-lock"></i>CONFIGURAÇÃO</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                    <span class="bx bx-lock"></span></div>
            </div>
            <div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
                <div class="single-features mb-0">
                    <h3><i class="bx bx-certification"></i>PROJETOS</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                    <span class="bx bx-certification"></span></div>
            </div>
        </div>
    </div>
    <div class="lines">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
</section>

<div class="partner-area ptb-100">
    <div class="container"></div>
</div>


<section class="security-area pb-70">
    <div class="container">
        <div class="section-title">
            <h2>Ipsum Dolor Sit Amet</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima sapiente.</p>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-bug"></i>
                    <h3>Ipsum Dolor Sit Amet</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-content"></i>
                    <h3>Ipsum Dolor Sit Amet</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-support"></i>
                    <h3>Ipsum Dolor Sit Amet</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-profile"></i>
                    <h3>Ipsum Dolor Sit Amet</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="approach-area pb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="approach-img"><img src="img/approach-img.jpg" alt="Image"/></div>
            </div>
            <div class="col-lg-6">
                <div class="approach-content">
                    <h2>Ipsum Dolor Sit Amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv</p>
                    <ul>
                        <li><i class="flaticon-cyber"></i>
                            <h3>Ipsum Dolor Sit Amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                        </li>
                        <li><i class="flaticon-cyber-security"></i>
                            <h3>Ipsum Dolor Sit Amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                        </li>
                        <li><i class="flaticon-profile"></i>
                            <h3>Ipsum Dolor Sit Amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="solutions-area pb-70">
    <div class="container">
        <div class="section-title">
            <h2>Ipsum Dolor Sit Amet</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima.</p>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="single-solutions solutions-time-bg-1">
                    <div class="solutions-content">
                        <h3>Ipsum Dolor Sit Amet</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolorer</p>
                        <a class="read-more" href="service-details.html">Read More</a></div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-2">
                            <div class="solutions-content">
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-3">
                            <div class="solutions-content">
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-4">
                            <div class="solutions-content">
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-5">
                            <div class="solutions-content">
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="single-solutions solutions-time-bg-6">
                    <div class="solutions-content">
                        <h3>Ipsum Dolor Sit Amet</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolorer</p>
                        <a class="read-more" href="service-details.html">Read More</a></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="electronic-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <div class="electronic-content">
                    <h2>Ipsum Dolor Sit Amet</h2>
                    <div class="electronic-tab-wrap">
                        <div class="tab electronic-tab">
                            <ul class="tabs">
                                <li class="current">INSTALAÇÃO</li>
                                <li>CONFIGURAÇÃO</li>
                                <li>GDPR</li>
                                <li>PROJETOS</li>
                                <li>PORTARIA VIRTUAL</li>
                                <li>CÂMERAS DE RUA</li>
                            </ul>
                            <div class="tab_content">
                                <div id="tab1" class="tabs_item">
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo ducimus vero, vero corporis voluptates beatae pariatur laudantium, fugiat illum ab deserunt nostrum aliquid quisquam esse? Voluptatibus quia velit numquam esse porro ipsum dolor, sit amet consectetur adipisicing elit. Illo ducimus vero, corporis.</p>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis, soluta, aspernatur dolorum sequi quisquam ullam in pariatur nihil dolorem cumque excepturi totam. Qui excepturi quasi cumque placeat fuga. Ea, eius?</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab2" class="tabs_item">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum</p>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab3" class="tabs_item">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab4" class="tabs_item">
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                                <div id="tab5" class="tabs_item">
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique</p>
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                                    <a class="default-btn" href="about.html">Learn About</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-area ptb-100">
    <div class="container">
        <div class="section-title white-title">
            <h2>Ipsum Dolor Sit Amet</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima.</p>
        </div>
    </div>
</section>

<section class="complete-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="complete-img"></div>
            </div>
            <div class="col-lg-6">
                <div class="complete-content">
                    <h2>Ipsum Dolor Sit Amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor elit incididunt ut labore et dolore magna aliqua. Quis ipsum</p>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security"><i class="flaticon-order"></i>
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security"><i class="flaticon-anti-virus-software"></i>
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security mb-0 mb-rs-need"><i class="flaticon-scientist"></i>
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="single-security mb-0"><i class="flaticon-technical-support"></i>
                                <h3>Ipsum Dolor Sit Amet</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="complete-shape"><img src="img/complete-shape.png" alt="Image"/></div>
</section>



@endsection
