@extends('template.layout')
@section('conteudo')

<div class="page-title-area bg-22">
    <div class="container">
        <div class="page-title-content">
            <h2>Serviço Um RE Sercuryti</h2>
            <ul>
                <li><a href="home">Home</a></li>
                <li>Serviço Um</li>
            </ul>
        </div>
    </div>
</div>

<div class="pt-100">
    <section class="approach-area pb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="approach-img"><img src="img/approach-img.jpg" alt="Image"/></div>
                </div>
                <div class="col-lg-6">
                    <div class="approach-content">
                        <h2>Our Approach To Security</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsumv
                        </p>
                        <ul>
                            <li><i class="flaticon-cyber"></i>
                                <h3>Secure by Design</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut suspendisse ultrices</p>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="security-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <h2>Complete Website Security</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima sapiente.</p>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-bug"></i>
                    <h3>Malware Detection Removal</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-content"></i>
                    <h3>Content Delivery Network</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-support"></i>
                    <h3>24/7 Cyber Security Support</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-security"><i class="flaticon-profile"></i>
                    <h3>Managed Web Application</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="solutions-area section-width pb-100">
    <div class="container">
        <div class="section-title">
            <h2>High-Performance Solutions</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima.</p>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="single-solutions solutions-time-bg-1">
                    <div class="solutions-content">
                        <h3>Secure Managed IT</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolorer</p>
                        <a class="read-more" href="service-details.html">Read More</a></div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-2">
                            <div class="solutions-content">
                                <h3>Compliance</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-3">
                            <div class="solutions-content">
                                <h3>Cyber Security</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-4">
                            <div class="solutions-content">
                                <h3>Disaster Planning</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="single-solutions solutions-time-bg-5">
                            <div class="solutions-content">
                                <h3>Secure By Design</h3>
                                <p>Lorem ipsum dolor sit amet sed, consectetur adipiscing elit do</p>
                                <a class="read-more" href="service-details.html">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="single-solutions solutions-time-bg-6">
                    <div class="solutions-content">
                        <h3>Secure Awareness Training</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolorer</p>
                        <a class="read-more" href="service-details.html">Read More</a></div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="client-area ptb-100">
    <div class="container">
        <div class="section-title white-title">
            <h2>What Client’s Say About Us</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus quam neque quibusdam corrupti aspernatur corporis alias nisi dolorum expedita veritatis voluptates minima.</p>
        </div>
    </div>
</section>



@endsection
