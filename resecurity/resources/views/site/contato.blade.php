@extends('template.layout')
@section('conteudo')

<div class="page-title-area bg-22">
    <div class="container">
        <div class="page-title-content">
            <h2>Contato RE Security</h2>
            <ul>
                <li><a href="home">Home</a></li>
                <li>Contato</li>
            </ul>
        </div>
    </div>
</div>

<div class="contact-info-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 p-0">
                <div class="single-contact-info"><i class="bx bx-location-plus"></i>
                    <h3>New York</h3>
                    <p>88 Flower Avenue. Kingdom St. New York</p>
                    <a href="mailto:hello@pisa.com">Email: info@pisa.com</a><a href="tel:+822456974">+822456974</a></div>
            </div>
            <div class="col-lg-6 p-0">
                <div class="single-contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.1583091352!2d-74.11976373946234!3d40.69766374859258!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sbd!4v1588019781257!5m2!1sen!2sbd"></iframe>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="faq-contact-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contact-wrap">
                    <div class="contact-form">
                        <div class="section-title"><span class="top-title">Contact Us</span>
                            <h2>Drop us a message for any query</h2>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque quibusdam deleniti porro praesentium. Aliquam minus quisquam velit in at nam.</p>
                        </div>
                        <form id="contactForm">
                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" required="" placeholder="Your Name"/>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control" required="" placeholder="Your Email"/>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="phone_number" id="phone_number" required="" class="form-control" placeholder="Your Phone"/>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="msg_subject" id="msg_subject" class="form-control" required="" placeholder="Your Subject"/>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <textarea name="message" class="form-control" id="message" cols="30" rows="8" required="" placeholder="Your Message"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="default-btn page-btn">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
