<!doctype html>
<html lang="pt-br">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#025959">
    <meta name="description" content="Supremo Digital é um estúdio de Criação de sites, Marketing digital, Desenvolvimento de Sistema e App, sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital.">
    <meta name="author" content="Alessandro Palmeira | Icaro Sammer">
    <meta name="robots" content="index">

    <!-- Facebook e Twitter compartilhamento -->
    <meta property="og:image" content="Supremo Digital"/>
    <meta property="og:url" content="https://supremodigital.com.br/"/>
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="Supremo Digital - Criação de Sites - Desenvolvimento de site - Marketing Digital - Sistema Web"/>
    <meta property="og:title" content="Supremo Digital - Criação de sites, Marketing digital, Desenvolvimento de Sistema e App"/>
    <meta property="og:description" content="Supremo Digital é um estúdio de Criação de sites, Marketing digital, Desenvolvimento de Sistema e App, sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital."/>
    <meta name="twitter:image" content=""/>
    <meta name="twitter:url" content="https://supremodigital.com.br/"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:title" content="Supremo Digital - Criação de Sites - Desenvolvimento de site - Marketing Digital - Sistema Web"/>
    <meta name="twitter:description" content="Supremo Digital é um estúdio de Criação de sites, Marketing digital, Desenvolvimento de Sistema e App, sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital."/>


<title>Supremo Digital - Aplicação web - Criação de Sites - Desenvolvimento de site - Marketing Digital - Aplicação Web</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/ico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/ico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/ico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/ico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/ico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/ico/favicon-16x16.png">
    <link rel="manifest" href="img/ico/manifest.json">
    <meta name="msapplication-TileColor" content="#025959">
    <meta name="msapplication-TileImage" content="img/ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#025959">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/supremo.css') }}">
</head>

<body>

    <header class="header-area fixed-top">
        <div class="top-header-area">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-sm-8">
                        <ul class="header-content-left">
                            <li><a href="mailto:elisangela.gomes@resecurity.com.br"><i class="bx bx-envelope"></i>Email: elisangela.gomes@resecurity.com.br</a></li><!--
                            <li><i class="bx bx-location-plus"></i>658 Lane Drive St. California</li>-->
                        </ul>
                    </div>
                    <div class="col-lg-6 col-sm-4">
                        <ul class="header-content-right">
                            <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                            <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                            <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-area">
            <div id="navbar" class="navbar-area">
                <div class="main-nav">
                    <div class="container-fluid">
                        <nav class="navbar navbar-expand-md navbar-light"><a class="navbar-brand" href="/">
                            <img src="img/logo-resecurity.svg" alt="logo" class="logo"/></a>
                            <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="icon-bar top-bar"></span><span class="icon-bar middle-bar"></span><span class="icon-bar bottom-bar"></span></button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav m-auto">
                                    <li class="nav-item"><a class="nav-link" href="home">HOME</a></li>
                                    <li class="nav-item"><a class="nav-link" href="sobre">SOBRE</a></li>
                                    <li class="nav-item"><a class="nav-link" href="servico">SERVIÇOS<i class="bx bx-chevron-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item"><a class="nav-link" href="servico-um">SERVIÇOS 1</a></li>
                                            <li class="nav-item"><a class="nav-link" href="servico">SERVIÇOS 2</a></li>
                                            <li class="nav-item"><a class="nav-link" href="servico">SERVIÇOS 3</a></li>
                                            <li class="nav-item"><a class="nav-link" href="servico">SERVIÇOS 4</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="cliente">CLIENTES</a></li>
                                    <li class="nav-item"><a class="nav-link" href="contato">CONTATO</a></li>
                                </ul>
                                <div class="others-option">
                                    <div class="get-quote"><a class="default-btn" href="orcamento">Orçamento</a></div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- CONTEUDO -->
    @yield('conteudo')


    <footer class="footer-top-area pt-100 pb-70 jarallax">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-widget contact">
                        <h3>Contato</h3>
                        <ul>
                            <li class="pl-0">
                                <a href="tel:+892-569-756">
                                <i class="bx bx-phone-call"></i>+55 11 5695 6756</a>
                            </li>
                            <li class="pl-0">
                                <a href="mailto:hello@pisa.com">
                                <i class="bx bx-envelope"></i>contato@resecurity.com.br</a>
                            </li>
                            <li><i class="bx bx-location-plus"></i>End.: </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-widget">
                        <h3>Serviços</h3>
                        <ul>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Serviços 1</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Serviços 2</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Serviços 3</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Serviços 4</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Serviços 5</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Serviços 6</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-widget">
                        <h3>Suporte</h3>
                        <ul>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Contato 1</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Contato 2</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Contato 3</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Contato 4</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Contato 5</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Contato 6</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="single-widget">
                        <h3>Links</h3>
                        <ul>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Links 1</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Links 2</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Links 3</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Links 4</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Links 5</a></li>
                            <li><a href="#"><i class="bx bx-chevrons-right"></i>Links 6</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <footer class="footer-bottom-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="copy-right">
                        <p>Copyright @<!-- -->2020<!-- --> RE Security. Desenvolvimento <a href="https://envytheme.com/" target="blank">Supremo Digital</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- SCRIPTS : begin -->
		<script src="js/jquery-3.5.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>


		<script src="js/supremo.js"></script>

</body>

</html>
